<?php require_once "./code.php";?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Activity s03</title>
</head>
<body>
	<h3>Person</h3>
	<p><?php echo $person->printName() ;?></p>

	<h3>Developer</h3>
	<p><?php echo $developer->printName() ;?></p>
	
	<h3>Engineer</h3>
	<p><?php echo $engineer->printName() ;?></p>

</body>
</html>