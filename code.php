<?PHP

class Person
{
	public $firstName;
	public $middleName;
	public $lastName;

	//Constructor
	public function __construct($firstName, $middleName, $lastName) {
		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;
	}

	//methods
	public function printName() {
		return "Person: \" Your full name is $this->firstName $this->middleName $this->lastName. \"";
	}
	
}

class Developer extends Person {

	public function printName() {
		return "Developer: \" Your name is $this->firstName $this->middleName $this->lastName and you are a developer. \"";
	}
}

class Engineer extends Person {

	public function printName() {
		return "Engineer: \" Your are an engineer named $this->firstName $this->middleName $this->lastName. \"";
	}
}

$person = new Person('John', 'Doe','Smith');

$developer = new Developer('Senku','Finch','Ishigami');

$engineer = new Engineer('Luffy', 'De', 'Monkey');